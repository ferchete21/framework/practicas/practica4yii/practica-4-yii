<!DOCTYPE html>
<?php
use yii\helpers\Url;
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="post" action="<?= yii\helpers\Url::to(['site/formulario']) ?>">
            <div class="form-group">
                <label for="iusuario">Nombre</label>
                <input type="text"class="form-control" id="iusuario" name="usuario">
            </div>
            <button class="btn btn-info">Enviar</button>
        </form>
    </body>
</html>
