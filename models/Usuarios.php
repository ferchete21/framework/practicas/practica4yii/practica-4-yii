<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Usuarios extends Model{
    public $nombre;
    public $apellidos;
    public $edad;
    public $email;
    public function rules() {
        return [
            [['nombre','edad','email'],'required','message'=>'{attribute} es obligatorio'],
            ['email', 'email','message'=>'Escribir correctamente el correo electrónico'],
            ['edad','number','min'=>18,'max'=>65,'tooBig'=>'Tienes que estar capacitado para ser explotado laboralmente','tooSmall'=>'Tienes que estar capacitado para ser explotado laboralmente']
        ];
    }
    public function attributeLabels() {
        return [
            'nombre'=> 'Nombre de usuario',
        ];
    }
    
    
}